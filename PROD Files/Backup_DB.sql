

DECLARE @fileDate VARCHAR(MAX) = CONVERT(varchar(20), GETDATE(), 112)
DECLARE @fileName VARCHAR(MAX) = '$(fileDir)' + '$(dbName)' + @fileDate + '.bak'

IF EXISTS (SELECT 1 FROM sys.databases WHERE name = '$(dbName)')
BEGIN

-- BACKUP DB 
BACKUP DATABASE $(dbName) to DISK = @fileName

END



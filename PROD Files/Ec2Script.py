import boto3, time, sys


DEVICE_NAME = '/dev/xvdf'
RC_INSTANCE_ID = 'i-ddd3a5c5'
RC_ROOT_VOL_ID = 'vol-2f6a1da7'
PROD_VOLUME_ID = 'vol-2b43d99d'
SNAP_DESC = 'Nuvolo Prod Data Backup Snapshot'
REGION = 'us-west-2a'

# Low-level connections
ec2 = boto3.resource('ec2')

# get all of the volumes of the instance that are not the root volumes
def getNonRootVolumes(instance, root_vol_id):
	nonRootVolumeList = []

	volumes = instance.volumes.all() # If you want to list out all volumes
	volumes = instance.volumes.filter(Filters=[{'Name': 'status', 'Values': ['in-use']}]) # if you want to list out only attached volumes
	for volume in volumes:

		if (volume.id == root_vol_id):
			continue

		nonRootVolumeList.append(volume)

	return nonRootVolumeList



def stopRcInstance(rc_instance):	
	print("Stopping RC Instance")
	if (rc_instance.state['Name'] != 'stopped'):
		rc_instance.stop()

	# wait for RC instance to stop
	while (rc_instance.state['Name'] != 'stopped'):
		print("reloading instance: " + rc_instance.state['Name'])
		rc_instance.reload()
		time.sleep(5)
	print("Instance with id {} is stopped".format(rc_instance.id))


def detachDataVolume(instance, volume):
	print("Detaching Volume")
	if (volume.state != "available"):
		detach_response = instance.detach_volume(Force=True, VolumeId=volume.id)

	print("volume with id {} detached".format(volume.id))


def detachVolumes(instance, volumeList):
	print("Detaching Old Volumes")
	for volume in volumeList:
		detachDataVolume(instance, volume)

def deleteVolumes(volumeList):
	print("Deleting Old Volumes")
	for volume in volumeList:
		if (volume.state != 'attached'):
			volume.delete()

# create snapshot from Prod volume
def createProdSnapshot():
	print("Creating Prod Snaspshot")
	snapshot = ec2.create_snapshot(VolumeId=PROD_VOLUME_ID, Description=SNAP_DESC)
	# wait until snapshot creation is complete
	while (snapshot.state != 'completed'):
		print("reloading snapshot: " + snapshot.state)
		snapshot.reload()
		time.sleep(5)
	print("snapshot with id {} created".format(snapshot.id))
	return snapshot

# create a new volume from the snapshot
def createVolume(snapshot):
	print("Creating New Volume")
	volume = ec2.create_volume(SnapshotId=snapshot.id, AvailabilityZone=REGION, VolumeType='gp2')
	volume.reload()
	# wait until volume is available
	while (volume.state != 'available'):
		print("reloading volume: " + volume.state)
		volume.reload()
		time.sleep(5)
	print("volume with id {} created".format(volume.id))
	return volume

def attachVolume(instance, volume, device_name):
	print("Attaching New Volume")
	attach_response = instance.attach_volume(VolumeId=volume.id, Device=device_name)
	print(attach_response['State'])

def process():

	# get RC instance
	rc_instance = ec2.Instance(RC_INSTANCE_ID)

	#stop RC instance
	stopRcInstance(rc_instance);

	# get all volumes form instance that should be detached and deleted
	nonRootVolumeList = getNonRootVolumes(rc_instance, RC_ROOT_VOL_ID)

	# detach old volumes from instance
	detachVolumes(rc_instance, nonRootVolumeList)

	# create snapshot from Prod volume
	snapshot = createProdSnapshot()

	# create a new volume from the snapshot
	new_volume = createVolume(snapshot)

	# attach the volume to the RC instance
	attachVolume(rc_instance, new_volume, DEVICE_NAME)

	# restart RC instance
	rc_instance.start()

	# delete the original snapshot
	print("Deleting Snapshot")
	snapshot.delete()

	# delete old volumes
	deleteVolumes(nonRootVolumeList)

	# update name of new volume

	print("Complete!")


if __name__ == '__main__':

	if (len(sys.argv) == 7):
		DEVICE_NAME = sys.argv[1]
		RC_INSTANCE_ID = sys.argv[2]
		RC_ROOT_VOL_ID = sys.argv[3]
		PROD_VOLUME_ID = sys.argv[4]
		SNAP_DESC = sys.argv[5]
		REGION = sys.argv[6]

	process();

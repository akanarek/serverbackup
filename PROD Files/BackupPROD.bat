
@ECHO OFF

rem get the number of parameters
set argC=0
for %%x in (%*) do Set /A argC+=1

set base_dir=%cd%\

rem Database configurations
set backup_dir=D:\DatabaseBackups\
set db_name=NUVOLO

set sql_filename=Backup_DB.sql
set sql_filepath=%base_dir%%sql_filename%
set server_name=.\SQLEXPRESS

rem AWS Configurations
set device_name=/dev/xvdf
set rc_instance_id=i-ddd3a5c5
set rc_root_volume_id=vol-2f6a1da7
set prod_volume_id=vol-2b43d99d
set snapshot_description="Nuvolo Prod Data Backup Snapshot"
set region=us-west-2a

set script_name=Ec2Script.py

rem set variables to command line input
if %argC% EQU 8 (
	rem db configs
	set backup_dir=%1
	set db_name=%2

	rem aws configs
	set device_name=%3
	set rc_instance_id=%4
	set rc_root_volume_id=%5
	set prod_volume_id=%6
	set snapshot_description=%7
	set region=%8
)

rem remove directory and files in it to save space
if exist %backup_dir% rmdir %backup_dir% /s /q

rem Create Directory if it doesn't exist
if not exist %backup_dir% mkdir %backup_dir%

rem backup DB
sqlcmd -E -S %server_name% -i "%sql_filepath%" -v dbName="%db_name%" fileDir="%backup_dir%"

rem AWS volume backup and restor
echo python %script_name% %device_name% %rc_instance_id% %rc_root_volume_id% %prod_volume_id% %snapshot_description% %region%
python %script_name% %device_name% %rc_instance_id% %rc_root_volume_id% %prod_volume_id% %snapshot_description% %region%

PAUSE
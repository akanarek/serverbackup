

@ECHO OFF

rem get the number of parameters
set argC=0
for %%x in (%*) do Set /A argC+=1

set base_dir=%cd%\

rem Database configurations
set backup_dir=D:\DatabaseBackups\
set db_name=NUVOLO
set sql_filename=Drop_Restore_DB.sql
set sql_filepath=%base_dir%%sql_filename%
set server_name=.\SQLEXPRESS

rem set variables to command line input
if %argC% EQU 2 (
	set backup_dir=%1
	set db_name=%2
)

rem remove and restore db with backup
sqlcmd -E -S %server_name% -i "%sql_filepath%" -v dbName="%db_name%" fileDir="%backup_dir%"

PAUSE


DECLARE @fileDate VARCHAR(MAX) = CONVERT(varchar(20), GETDATE(), 112)
DECLARE @fileName VARCHAR(MAX) = '$(fileDir)$(dbName)' + @fileDate + '.bak'

IF EXISTS (SELECT 1 FROM sys.databases WHERE name = '$(dbName)')
BEGIN

-- DROP DB
DECLARE @dropDbQuery VARCHAR(MAX) = 'DROP DATABASE $(dbName)'

EXEC(@dropDbQuery)

END


-- RESTORE DB

RESTORE DATABASE $(dbName) FROM DISK = @fileName

